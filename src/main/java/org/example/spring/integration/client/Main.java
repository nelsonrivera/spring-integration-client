/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.example.spring.integration.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author f3l1p3x
 */
public class Main {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        //String path = Main.class.getClassLoader().getResource("spring-integration-client.xml").getPath();
        //System.out.println("Path to context" + path);
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring-integration-client.xml");
        System.out.println("Context created");
        System.out.println("Getting nexgenGateway gateway");

        NexgenGateway gateway = applicationContext.getBean(NexgenGateway.class);

        System.out.println("Sending texto through gateway");
        byte[] result = gateway.sendPerson(args[0].getBytes());

        System.out.println("Connection result: " + new String(result));
    }

}
