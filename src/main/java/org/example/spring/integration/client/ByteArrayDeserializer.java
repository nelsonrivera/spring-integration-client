/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.example.spring.integration.client;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.springframework.integration.ip.tcp.serializer.AbstractByteArraySerializer;

/**
 *
 * @author f3l1p3x
 */
public class ByteArrayDeserializer extends AbstractByteArraySerializer 
{

    @Override
    public void serialize(byte[] t, OutputStream out) throws IOException {
        out.write(t);
        out.flush();
    }

    @Override
    public byte[] deserialize(InputStream in) throws IOException {
       byte[] bytes = new byte[maxMessageSize];
       
       
       in.read(bytes);
       
       return bytes;
       
    }
    
}
